<?php

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['verify' => true]);

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/profile/edit','HomeController@showEditProfileForm')->name('edit.profile');
Route::post('/profile/editing','HomeController@editingProfile')->name('editing.profile');

Route::get('/client/search','ClientController@search')->name('client.search');
Route::get('/client/new','ClientController@new')->name('client.new');
Route::get('/client/edit/{client_id}','ClientController@edit')->name('client.edit');
Route::get('/client/delete/{client_id}','ClientController@delete')->name('client.delete');

Route::post('/client/register','ClientController@register')->name('client.register');
Route::post('/client/editing','ClientController@editing')->name('client.editing');

Route::get('/user/search','UserController@search')->name('user.search');
Route::get('/user/new','UserController@new')->name('user.new');
Route::get('/user/delete/{user_id}','UserController@delete')->name('user.delete');

Route::post('/user/register','UserController@register')->name('user.register');