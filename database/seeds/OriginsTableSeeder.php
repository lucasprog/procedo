<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OriginsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    
        $insert = array(
            [ 'name' => 'Site' ],
            [ 'name' => 'Boca a Boca' ],
            [ 'name' => 'Facebook' ],
            [ 'name' => 'Indicação' ]
        );
        
        foreach( $insert as $origin){

            DB::table('origins')->insert( $origin );

        }
        
    }
}
