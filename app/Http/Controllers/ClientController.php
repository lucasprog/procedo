<?php

namespace procedo\Http\Controllers;

use Illuminate\Http\Request;
use procedo\User;
use procedo\Clients;
use procedo\Origins;
use Illuminate\Support\Facades\Validator;

class ClientController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function search(Request $request, Origins $originModel){

        $clientsModel = auth()->user()->clients();
        
        if( isset($request->name) )
        $clientsModel->where('name',$request->name);
        
        if( isset($request->situation) )
        $clientsModel->where('situation',$request->situation);
        
        if( isset($request->state) )
            $clientsModel->where('state',$request->state);
            
        if( isset($request->city) )
            $clientsModel->where('city',$request->city);

        if( isset($request->origin) ){
            
            foreach( $request->origin as $origin ){
                
                $clientsModel->where('clients_origins.origins_id','=',$origin);
                
            }

        }
        
        // $clientsModel->where('user_id',auth()->user()->id);
            
        $params = [
            'clients' => $clientsModel->paginate(15) ,
            'origins' => $originModel->all()
        ];

        return view('client.search')->with($params);

    }

    public function new(Origins $originModel){

        $params = [
            'origins' => $originModel->all()
        ];

        return view('client.new')->with($params);

    }

    public function edit($client_id, Clients $clientsModel, Origins $originModel){

        $params = [
            'origins' => $originModel->all(),
            'client'    => $clientsModel->find($client_id)
        ];

        return view('client.edit')->with($params);

    }

    public function delete( $client_id, Clients $clientsModel){
        
        if( isset($client_id) ){

            $client = $clientsModel->find($client_id);

            if( $client->delete() ){

                return redirect()->route('client.search')->with('Cliente excluído com sucesso!');

            }else{

                return redirect()->back()->withErrors(['Oops, ocorreu um erro ao tentar excluír cliente!']);

            }

        }else{

            return redirect()->back()->withErrors(['Oops, ocorreu um erro ao tentar excluír cliente. informe o código do cliente!']);

        }

    }

    public function register(Request $request, Clients $clientsModel){

        $post = $request->all();

        $validator =  Validator::make($post, [
            'name'              => 'required',
            'email'              => 'required|email',
            'cnpj'                 => 'required',
            'phone'             => 'required',
            'origin'              => 'required',
            'state'                => 'required',
            'city'                   => 'required'
        ],[
            'name.required'             => 'Oops, por favor digite o nome',
            'email.required'              => 'Oops, por favor digite o email',
            'email.email'                    => 'Oops, email inválido',
            'cnpj.required'                 => 'Oops, por favor digite o CNPJ',
            'phone.required'             => 'Oops, digite o número de telefone',
            'origin.required'              => 'Oops, selecione a origem do cliente',
            'state.required'                => 'Oops, selecione o estado',
            'city.required'                   => 'Oops, selecione a cidade'
        ]);

        if ($validator->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $create = $clientsModel->create([
            'name'              => $post['name'],
            'email'              => $post['email'],
            'cnpj'                 => $post['cnpj'],
            'phone'             => $post[ 'phone'],
            'state'                => $post['state'],
            'city'                   => $post['city'],
            'situation'         => $post['situation'],
            'observation'   => $post['observation'],
            'user_id'            => auth()->user()->id
        ]);

        if( $create ){

            $create->origins()->attach($request['origin']);
            
            return redirect()->route('client.search')->with('Cliente cadastrado com sucesso!');

        }else{

            return redirect()->back()->withErrors(['Oops, ocorreu um erro ao cadastrar cliente. Verifque se o dados estão preenchidos corretamente.!']);

        }

    }

    public function editing(Request $request, Clients $clientsModel){

        $post = $request->all();

        $validator =  Validator::make($post, [
            'name'              => 'required',
            'email'              => 'required|email',
            'cnpj'                 => 'required',
            'phone'             => 'required',
            'origin'              => 'required',
            'state'                => 'required',
            'city'                   => 'required'
        ],[
            'name.required'             => 'Oops, por favor digite o nome',
            'email.required'              => 'Oops, por favor digite o email',
            'email.email'                    => 'Oops, email inválido',
            'cnpj.required'                 => 'Oops, por favor digite o CNPJ',
            'phone.required'             => 'Oops, digite o número de telefone',
            'origin.required'              => 'Oops, selecione a origem do cliente',
            'state.required'                => 'Oops, selecione o estado',
            'city.required'                   => 'Oops, selecione a cidade'
        ]);

        if ($validator->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $client = $clientsModel->find($request['client_id']);

        $update = $clientsModel
                            ->where('id',$request['client_id'])
                            ->where('user_id',auth()->user()->id)
                            ->update([
            'name'              => $post['name'],
            'email'              => $post['email'],
            'cnpj'                 => $post['cnpj'],
            'phone'             => $post[ 'phone'],
            'state'                => $post['state'],
            'city'                   => $post['city'],
            'situation'         => $post['situation'],
            'observation'   => $post['observation']
        ]);

        if( $update ){

            $client->origins()->detach();

            $client->origins()->attach($request['origin']);
            
            return redirect()->route('client.search')->with('Dados do cliente atualizado com sucesso!');

        }else{

            return redirect()->back()->withErrors(['Oops, ocorreu um erro ao atualizar dados od cliente. Verifque se o dados estão preenchidos corretamente.!']);

        }

    }

}