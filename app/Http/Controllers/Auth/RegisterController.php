<?php

namespace procedo\Http\Controllers\Auth;

use procedo\User;
use procedo\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return  Validator::make($data, [
            'name'          => 'required',
            'sex'               => 'required',
            'phone'         => 'required',
            'state'            => 'required',
            'city'               => 'required',
            'email'           => 'required|email|unique:users',
            'password'   => 'required|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/',
        ],[
            'name.required' => 'Oops, informe o nome',
            'sex.required' => 'Oops, informe o sexo',
            'phone.required' => 'Oops, informe o telefone',
            'state.required' => 'Oops,  selecione o estado',
            'city.required' => 'Oops, selecione a cidade',
            'email.required' => 'Oops, informe o email',
            'email.email' => 'Oops, email inválido!',
            'password.required' => 'Oops, informe a senha',
        ]);

    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \procedo\User
     */
    protected function create(array $data)
    {

        $data_insert = [
            'name' => $data['name'],
            'sex' => $data['sex'],
            'phone' => $data['phone'],
            'state' => $data['state'],
            'city' => $data['city'],
            'email' => $data['email'],
            'password'   => bcrypt($data['password']),
            'situation' => false
        ];

        return User::create($data_insert);

    }

    
}
