<?php

namespace procedo\Http\Controllers;

use Illuminate\Http\Request;
use procedo\User;
use Illuminate\Support\Facades\Validator;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function showEditProfileForm()
    {
        return view('auth.edit');
    }

    public function editingProfile( Request $request, User $userModel ){

        $validator =  Validator::make($request->all(), [
            'name'          => 'required|string|max:255',
            'sex'               => 'required',
            'phone'         => 'required|string',
            'state'            => 'required|string',
            'city'               => 'required|string'
        ]);

        if ($validator->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $data_update = [
            'name'          => $request['name'],
            'sex'               =>  $request['sex'],
            'phone'         =>  $request['phone'],
            'state'            =>  $request['state'],
            'city'               =>  $request['city']
        ];

        if(  isset($request['password_new']) && $request['password_new'] != null && count($request['password_new']) > 0 ){

            if( $request['password_new'] == $request['password_confirmation_new'] ){
                
                $validator =  Validator::make($request->all(), [
                    'password_new' => 'regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/'
                ],[
                    'password_new.regex' => 'Nova senha esta no formato inválido!'
                ]);
        
                if ($validator->fails()) {
                    return redirect()
                                ->back()
                                ->withErrors($validator)
                                ->withInput();
                }

                $data_update['password'] = bcrypt($request['password_new']);

            }else{

                return redirect()
                        ->back()
                        ->withErrors(['Confirmação de senha inválida!'])
                        ->withInput();

            }

        }

        $update = $userModel->where('id',$request['user_id'])->update($data_update);

        if( $update ){

            return redirect()->back()->with('Dados atualizado com sucesso!');

        }else{

            return redirect()
                        ->back()
                        ->withErrors(['Oops, erro ao atualizar dados!']);

        }

    }
}
