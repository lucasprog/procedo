<?php

namespace procedo\Http\Controllers;

use Illuminate\Http\Request;
use procedo\User;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function search(Request $request, User $userModel){
        
        if( isset($request->name) )
            $userModel->where('name',$request->name);
        
        if( isset($request->situation) )
            $userModel->where('situation',$request->situation);
        
        if( isset($request->state) )
            $userModel->where('state',$request->state);
            
        if( isset($request->city) )
            $userModel->where('city',$request->city);
        
        // $clientsModel->where('user_id',auth()->user()->id);
            
        $params = [
            'users' => $userModel->paginate(15) 
        ];

        return view('user.search')->with($params);

    }

    public function new(){

        return view('user.new');

    }

    public function register(Request $request, User $userModel){

        $post = $request->all();

        $validator = Validator::make($post, [
            'name'          => 'required',
            'sex'               => 'required',
            'phone'         => 'required',
            'state'            => 'required',
            'city'               => 'required',
            'email'           => 'required|email|unique:users',
            'password'   => 'required|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{6,}$/',
        ],[
            'name.required' => 'Oops, informe o nome',
            'sex.required' => 'Oops, informe o sexo',
            'phone.required' => 'Oops, informe o telefone',
            'state.required' => 'Oops,  selecione o estado',
            'city.required' => 'Oops, selecione a cidade',
            'email.required' => 'Oops, informe o email',
            'email.email' => 'Oops, email inválido!',
            'password.required' => 'Oops, informe a senha',
        ]);

        if ($validator->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($validator)
                        ->withInput();
        }

        $data_insert = [
            'name' => $post['name'],
            'sex' => $post['sex'],
            'phone' => $post['phone'],
            'state' => $post['state'],
            'city' => $post['city'],
            'email' => $post['email'],
            'password'   => bcrypt($post['password']),
            'situation' => $post['situation']
        ];

        $create = $userModel->create($data_insert);

        if( $create ){
            
            return redirect()->route('user.search')->with('Usuário cadastrado com sucesso!');

        }else{

            return redirect()->back()->withErrors(['Oops, ocorreu um erro ao cadastrar usuário. Verifque se o dados estão preenchidos corretamente.!']);

        }

    }

    public function delete( $user_id, user $userModel){
        
        if( isset($user_id) ){

            $user = $userModel->find($user_id);

            if( $user->delete() ){

                return redirect()->route('user.search')->with('Usuário excluído com sucesso!');

            }else{

                return redirect()->back()->withErrors(['Oops, ocorreu um erro ao tentar excluír usuário!']);

            }

        }else{

            return redirect()->back()->withErrors(['Oops, ocorreu um erro ao tentar excluír usuário. informe o código do usuário!']);

        }

    }

}
