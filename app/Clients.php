<?php

namespace procedo;

use Illuminate\Database\Eloquent\Model;
use procedo\Origins;
use procedo\User;

class Clients extends Model
{
    
    protected $fillable = [
        'name', 'email', 'cnpj','phone','state','city','situation','observation','user_id'
    ];

    public function origins(){

        return $this->belongsToMany(Origins::class);

    }

    public function user(){

        return $this->belongsTo(User::class);

    }


}
