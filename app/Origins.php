<?php

namespace procedo;

use Illuminate\Database\Eloquent\Model;
use procedo\Clients;

class Origins extends Model
{
    
    public function user(){

        return $this->belongsToMany(Clients::class);

    }

}
