@extends('layouts.app')

@section('content')

    <div id="page-search">

        <div class="container">

            <div class="row">

                <div class="col-md-12">

                    <div class="panel panel-default">

                        <div class="panel-heading"> <i class="fa fa-search"></i> Buscar clientes</div>

                        <div class="panel-body">

                            <div class="row">

                                    <div class="col-md-12">

                                        <form  class="form-horizontal" action="{{ route('client.search') }}" method="get">

                                            {{-- Alerta Sucesso  --}}
                                            @if(session()->has('message'))
            
                                            <div class="alert alert-success">
                                                {{ session()->get('message') }}
                                            </div>
               
                                            @endif
               
                                            {{-- Alerta Erro --}}
                                            @if ($errors->any())
                                                <div class="alert alert-danger">
                                                    <ul>
                                                        @foreach ($errors->all() as $error)
                                                            <li>{{ $error }}</li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            @endif

                                            <div class="form-group">
                                                {{--  Nome  --}}
                                                <label for="name" class="pull-left  col-md-offset-1 control-label">Nome</label>
                                                
                                                <div class="col-md-2">
                                                    
                                                    <input type="text" name="name" class="form-control" placeholder="Buscar por nome">
                
                                                </div>
                                                
                                                {{--  Situação  --}}
                                                <label for="situation" class="pull-left control-label">Situação</label>
                                                
                                                <div class="col-md-2">
                                                    
                                                    <select name="situation" id="situation" class="form-control">
                                                        <option value="">Buscar por Situação</option>
                                                        <option value="1">Ativo</option>
                                                        <option value="0">Inativo</option>
                                                    </select>
                
                                                </div>

                                                {{--  Origem  --}}
                                                <label for="origin" class="pull-left control-label">Origem</label>
                                                
                                                <div class="col-md-2">
                                                    
                                                    <select name="origin[]" id="origin" class="form-control select2" multiple>
                                                        <option value="">Buscar por Situação</option>
                                                    
                                                        @foreach ($origins as $origin )
                                                            <option value="{{ $origin->id}}">{{ $origin->name}}</option>                                                            
                                                        @endforeach

                                                    </select>
                
                                                </div>

                                            </div>
                                            <div class="form-group">

                                                 {{--  Estado  --}}
                                                 <label for="state" class="pull-left  col-md-offset-1 control-label">Estado</label>
                                                
                                                 <div class="col-md-2">
                                                     
                                                     <select name="state" id="state" class="form-control">
                                                         <option value="">Buscar por Estado</option>
                                                     </select>
                 
                                                 </div>

                                                  {{--  Cidade  --}}
                                                <label for="city" class="pull-left control-label">Cidade</label>
                                                
                                                <div class="col-md-2">
                                                    
                                                    <select name="city" id="city" class="form-control">
                                                        <option value="">Buscar por Cidade</option>
                                                    </select>
                
                                                </div>

                                                <div class="col-md-3">
                                                    <button class="btn btn-success">
                                                        Buscar <i class="fa fa-search"></i>
                                                    </button>
                                                </div>

                                            </div>

                                        </form>

                                    </div>

                            </div>

                            <div class="row">

                                <div class="col-md-12">

                                    <table class="table">
                                        <thead>
                                            <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Nome</th>
                                            <th scope="col">Email</th>button
                                            <th scope="col">CNPJ</th>
                                            <th scope="col">Telefone</th>
                                            <th scope="col">Editar</th>
                                            <th scope="col">Excluir</th>
                                            </tr>
                                        </thead>
                                        <tbody>
        
                                            @foreach($clients as $client)
                                                <tr>
                                                    <th scope="row">{{ $client->id }}</th>
                                                    <td>{{ $client->name }}</td>
                                                    <td>{{ $client->email }}</td>
                                                    <td>{{ $client->cnpj }}</td>
                                                    <td>{{ $client->phone }}</td>
                                                    
                                                    <td>
                                                        <a href="{{ route('client.edit',$client->id)}}" class="btn  btn-warning">
                                                            <i class="fa fa-edit"></i> Editar
                                                        </a>
                                                    </td>

                                                    <td>
                                                        <button class="btn btn-danger btn-delete" data-url="{{route('client.delete',$client->id)}}">
                                                            <i class="fa fa-times"></i> Excluir
                                                        </button>
                                                    </td>
                                                </tr>
                                            @endforeach
        
                                        </tbody>
                                    </table>
        
                                    {{ $clients->links() }}

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

    @include('modal.modal-delete')

@endsection