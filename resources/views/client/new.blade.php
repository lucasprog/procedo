@extends('layouts.app')

@section('content')

<div id="page-register">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading"> <i class="fa fa-user"></i> Novo cliente</div>
                        
                        <div class="panel-body">
                            <form class="form-horizontal" method="POST" action="{{ route('client.register') }}">
                                {{ csrf_field() }}

                                 {{-- Alerta Sucesso  --}}
                                 @if(session()->has('message'))

                                 <div class="alert alert-success">
                                     {{ session()->get('message') }}
                                 </div>

                             @endif

                             {{-- Alerta Erro --}}
                             @if ($errors->any())
                                 <div class="alert alert-danger">
                                     <ul>
                                         @foreach ($errors->all() as $error)
                                             <li>{{ $error }}</li>
                                         @endforeach
                                     </ul>
                                 </div>
                             @endif
                            
                            <div class="form-group{{ $errors->has('situation') ? ' has-error' : '' }}">
                                <label for="situation" class="col-md-4 control-label">Situação</label>
                                
                                <div class="col-md-1">
                                    
                                    Ativo <input type="radio" name="situation" value="1" {{ old('situation') == 1? 'checked' : '' }}>

                                    @if ($errors->has('situation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('situation') }}</strong>
                                    </span>
                                    @endif
                                </div>

                                <div class="col-md-1">
                                    
                                    Inativo <input type="radio" name="situation" value="0" {{ old('situation') == 0? 'checked' : '' }}>

                                    @if ($errors->has('situation'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('situation') }}</strong>
                                    </span>
                                    @endif
                                </div>

                            </div>

                            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                <label for="name" class="col-md-4 control-label">Nome</label>
                                
                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus placeholder="Digite seu nome">

                                    @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>
                            
                            <div class="form-group{{ $errors->has('cnpj') ? ' has-error' : '' }}">
                                <label for="cnpj" class="col-md-4 control-label">CNPJ</label>
                                
                                <div class="col-md-6">

                                    <input type="text" name="cnpj" class="form-control mask-cnpj" value="{{ old('cnpj') }}" required placeholder="Digite o CNPJ">
                                    
                                    @if ($errors->has('cnpj'))
                                    <span class="help-block">
                                            <strong>{{ $errors->first('cnpj') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            
                            <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                                <label for="phone" class="col-md-4 control-label">Telefone</label>

                                <div class="col-md-6">
                                    <input id="phone" type="text" class="form-control mask-tel" name="phone" value="{{ old('phone') }}" required  placeholder="Digite seu número de telefone">
                                    
                                    @if ($errors->has('phone'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('phone') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('origin') ? ' has-error' : '' }}">
                                    <label for="origin" class="col-md-4 control-label">Origem</label>
                                    
                                    <div class="col-md-6">
    
                                        <select name="origin[]" id="origin" class="form-control select2" multiple="multiple">
                                            <option value="">Selecione a origem</option>

                                            @foreach( $origins as $origin)
                                                <option value="{{ $origin->id}}"> {{ $origin->name}} </option>
                                            @endforeach

                                        </select>
                                        
                                        @if ($errors->has('origin'))
                                        <span class="help-block">
                                                <strong>{{ $errors->first('origin') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('state') ? ' has-error' : '' }}">
                                    <label for="state" class="col-md-4 control-label">Estado</label>
                                    
                                    <div class="col-md-6">
    
                                        <select name="state" id="state" class="form-control">
                                            <option value="">Selecione o estado</option>
                                        </select>
                                        
                                        @if ($errors->has('state'))
                                        <span class="help-block">
                                                <strong>{{ $errors->first('state') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                                    <label for="city" class="col-md-4 control-label">Cidade</label>
                                    
                                    <div class="col-md-6">
    
                                        <select name="city" id="city" class="form-control">
                                            <option value="">Selecione a cidade</option>
                                        </select>
                                        
                                        @if ($errors->has('city'))
                                        <span class="help-block">
                                                <strong>{{ $errors->first('city') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">E-mail</label>
                                
                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required  placeholder="Digite seu e-mail">

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                        @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('observation') ? ' has-error' : '' }}">
                                <label for="observation" class="col-md-4 control-label">Observação</label>
                                
                                <div class="col-md-6">
                                    <textarea name="observation" id="observation" cols="30" rows="10" class="form-control" placeholder="Digite sua observação"></textarea>

                                    @if ($errors->has('observation'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('observation') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-success">
                                        Cadastrar <i class="fa fa-save"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
