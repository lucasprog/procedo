@extends('layouts.app')

@section('content')

    <div id="page-search">

        <div class="container">

            <div class="row">

                <div class="col-md-12">

                    <div class="panel panel-default">

                        <div class="panel-heading"> <i class="fa fa-search"></i> Buscar usuários</div>

                        <div class="panel-body">

                            <div class="row">

                                    <div class="col-md-12">

                                        <form  class="form-horizontal" action="{{ route('user.search') }}" method="get">

                                            {{-- Alerta Sucesso  --}}
                                            @if(session()->has('message'))
            
                                            <div class="alert alert-success">
                                                {{ session()->get('message') }}
                                            </div>
               
                                            @endif
               
                                            {{-- Alerta Erro --}}
                                            @if ($errors->any())
                                                <div class="alert alert-danger">
                                                    <ul>
                                                        @foreach ($errors->all() as $error)
                                                            <li>{{ $error }}</li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            @endif

                                            <div class="form-group">
                                                {{--  Nome  --}}
                                                <label for="name" class="pull-left  col-md-offset-1 control-label">Nome</label>
                                                
                                                <div class="col-md-2">
                                                    
                                                    <input type="text" name="name" class="form-control" placeholder="Buscar por nome">
                
                                                </div>
                                                
                                                {{--  Situação  --}}
                                                <label for="situation" class="pull-left control-label">Situação</label>
                                                
                                                <div class="col-md-2">
                                                    
                                                    <select name="situation" id="situation" class="form-control">
                                                        <option value="">Buscar por Situação</option>
                                                        <option value="1">Ativo</option>
                                                        <option value="0">Inativo</option>
                                                    </select>
                
                                                </div>

                                            </div>
                                            <div class="form-group">

                                                 {{--  Estado  --}}
                                                 <label for="state" class="pull-left  col-md-offset-1 control-label">Estado</label>
                                                
                                                 <div class="col-md-2">
                                                     
                                                     <select name="state" id="state" class="form-control">
                                                         <option value="">Buscar por Estado</option>
                                                     </select>
                 
                                                 </div>

                                                  {{--  Cidade  --}}
                                                <label for="city" class="pull-left control-label">Cidade</label>
                                                
                                                <div class="col-md-2">
                                                    
                                                    <select name="city" id="city" class="form-control">
                                                        <option value="">Buscar por Cidade</option>
                                                    </select>
                
                                                </div>

                                                <div class="col-md-3">
                                                    <button class="btn btn-success">
                                                        Buscar <i class="fa fa-search"></i>
                                                    </button>
                                                </div>

                                            </div>

                                        </form>

                                    </div>

                            </div>

                            <div class="row">

                                <div class="col-md-12">

                                    <table class="table">
                                        <thead>
                                            <tr>
                                            <th scope="col">#</th>
                                            <th scope="col">Nome</th>
                                            <th scope="col">Email</th>button
                                            <th scope="col">Telefone</th>
                                            <th scope="col">Excluir</th>
                                            </tr>
                                        </thead>
                                        <tbody>
        
                                            @foreach($users as $user)
                                                <tr>
                                                    <th scope="row">{{ $user->id }}</th>
                                                    <td>{{ $user->name }}</td>
                                                    <td>{{ $user->email }}</td>
                                                    <td>{{ $user->phone }}</td>

                                                    <td>
                                                        <button class="btn btn-danger btn-delete" data-url="{{route('user.delete',$user->id)}}">
                                                            <i class="fa fa-times"></i> Excluir
                                                        </button>
                                                    </td>
                                                </tr>
                                            @endforeach
        
                                        </tbody>
                                    </table>
        
                                    {{ $users->links() }}

                                </div>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

    @include('modal.modal-delete')

@endsection