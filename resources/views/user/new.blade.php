@extends('layouts.app')

@section('content')

<div id="page-register">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">Cadastre - se</div>
                        
                        <div class="panel-body">
                            <form class="form-horizontal" method="POST" action="{{ route('user.register') }}">
                                {{ csrf_field() }}
                                
                                <div class="form-group{{ $errors->has('situation') ? ' has-error' : '' }}">
                                    <label for="situation" class="col-md-4 control-label">Situação</label>
                                    
                                    <div class="col-md-1">
                                        
                                        Ativo <input type="radio" name="situation" value="1" {{ old('situation') == 1? 'checked' : '' }}>
    
                                        @if ($errors->has('situation'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('situation') }}</strong>
                                        </span>
                                        @endif
                                    </div>
    
                                    <div class="col-md-1">
                                        
                                        Inativo <input type="radio" name="situation" value="0" {{ old('situation') == 0? 'checked' : '' }}>
    
                                        @if ($errors->has('situation'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('situation') }}</strong>
                                        </span>
                                        @endif
                                    </div>
    
                                </div>

                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label for="name" class="col-md-4 control-label">Nome</label>
                                    
                                    <div class="col-md-6">
                                        <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus placeholder="Digite seu nome">

                                        @if ($errors->has('name'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('name') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                            </div>
                            
                            <div class="form-group{{ $errors->has('sex') ? ' has-error' : '' }}">
                                <label for="sex" class="col-md-4 control-label">Sexo</label>
                                
                                <div class="col-md-6">

                                    <select name="sex" id="sex" class="form-control">
                                        <option value="">Selecione o sexo</option>
                                        <option value="0" {{ (old("sex") == 0 && old("sex") != null ? "selected":"") }}>Masculino</option>
                                        <option value="1"  {{ (old("sex") == 1 && old("sex") != null ? "selected":"") }}>Feminino</option>
                                    </select>
                                    
                                    @if ($errors->has('sex'))
                                    <span class="help-block">
                                            <strong>{{ $errors->first('sex') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                            
                            <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                                <label for="phone" class="col-md-4 control-label">Telefone</label>

                                <div class="col-md-6">
                                    <input id="phone" type="text" class="form-control mask-tel" name="phone" value="{{ old('phone') }}" required  placeholder="Digite seu número de telefone">
                                    
                                    @if ($errors->has('phone'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('phone') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('state') ? ' has-error' : '' }}">
                                    <label for="state" class="col-md-4 control-label">Estado</label>
                                    
                                    <div class="col-md-6">
    
                                        <select name="state" id="state" class="form-control">
                                            <option value="">Selecione o estado</option>
                                        </select>
                                        
                                        @if ($errors->has('state'))
                                        <span class="help-block">
                                                <strong>{{ $errors->first('state') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group{{ $errors->has('city') ? ' has-error' : '' }}">
                                    <label for="city" class="col-md-4 control-label">Cidade</label>
                                    
                                    <div class="col-md-6">
    
                                        <select name="city" id="city" class="form-control">
                                            <option value="">Selecione a cidade</option>
                                        </select>
                                        
                                        @if ($errors->has('city'))
                                        <span class="help-block">
                                                <strong>{{ $errors->first('city') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>
                                
                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                <label for="email" class="col-md-4 control-label">E-mail</label>
                                
                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required  placeholder="Digite seu e-mail">

                                    @if ($errors->has('email'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                        @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                <label for="password" class="col-md-4 control-label">Senha</label>
                                
                                <div class="col-md-6">
                                    <input id="password" type="password" class="form-control" name="password" required  placeholder="Digite sua senha">

                                    @if ($errors->has('password'))
                                        <span class="help-block">
                                            <strong>{{ $errors->first('password') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="password-confirm" class="col-md-4 control-label">Confirmar Senha</label>

                                <div class="col-md-6">
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required  placeholder="Digite novamente sua senha">
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-success">
                                        Cadastrar <i class="fa fa-save"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
