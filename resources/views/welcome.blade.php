<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Procedo</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .social-network > a {
                display: inline-block;
                float: none;
                width: 30px;
                height: 30px;
                margin: 0 3px;
                background-color: #acacac;
                border-radius: 50%;
                text-decoration: none;
                line-height: 31px;
            }

            .social-network > a:hover{

                background-color: #0984e3;

            }

            .social-network > a > .fa{

                color: #fff;
                margin-top: 5px;

            }

            .m-b-md {
                margin-bottom: 30px;
            }

            .logo{
                max-width: 500px;
                width: 100%;;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>
                        <a href="{{ route('register') }}">Cadastrar - se</a>
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    <img src="{{ url('/imgs/logo.svg')}}" alt="Logo" class="logo">
                </div>

                <div class="social-network">

                    <a href="https://www.youtube.com/channel/UCT75ysxFquG5OgzAG7UyhkQ" target="_blank"><i class="fa fa-youtube"></i></a>
                    <a href="https://www.linkedin.com/company/procedotec/" target="_blank"><i class="fa fa-linkedin"></i></a>
                    <a href="https://www.facebook.com/procedotec" target="_blank"><i class="fa fa-facebook-f"></i></a>
                    <a href="https://www.instagram.com/procedotec/" target="_blank"><i class="fa fa-instagram"></i></a>

                </div>
            </div>
        </div>
    </body>
</html>
