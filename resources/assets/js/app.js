require('./bootstrap');
require('./jq_mask');
require('./select2');

$('.mask-tel').mask('(99) 9999 - 9999');
$('.mask-cnpj').mask('99.999.999/9999-99');

$('.select2').select2();

/**
 * Página de cadastro
*/
if( $('#main-content #page-register').length > 0 || $('#main-content #page-edit').length > 0 || $('#main-content #page-search').length > 0  ){

    let elemState = $("#state");
    let elemCity    = $("#city");

    $.get('https://servicodados.ibge.gov.br/api/v1/localidades/estados',function( result ){

        result.map( function(state,index){

            elemState.append('<option class="option-copy" data-id="'+state.id+'" value="'+state.sigla+'">'+state.nome+'</option>');

        });

        if( $('#main-content #page-edit').length > 0 ){

            let valueState = elemState.attr('data-value');
        
            elemState.find('option[value="'+valueState+'"]').prop('selected',true);

            let idState = elemState.find('option[value="'+valueState+'"]').attr('data-id');

            $.get('http://servicodados.ibge.gov.br/api/v1/localidades/estados/'+idState+'/municipios',function( result ){

                result.map( function(city,index){

                    elemCity.append('<option class="option-copy" data-id="'+city.id+'" value="'+city.nome+'">'+city.nome+'</option>');
        
                });
                
                let valueCity = elemCity.attr('data-value');
            
                elemCity.find('option[value="'+valueCity+'"]').prop('selected',true);

            });

        }

    },'json');

    elemState.on('change',function(){

        let valState = $(this).val();

        if( valState !== null && valState !== undefined && valState.length > 0 ){

            let idState = elemState.find('option[value="'+valState+'"]').attr('data-id');
            
            elemCity.find('.option-copy').remove();

            $.get('http://servicodados.ibge.gov.br/api/v1/localidades/estados/'+idState+'/municipios',function( result ){

                result.map( function(city,index){

                    elemCity.append('<option class="option-copy" data-id="'+city.id+'" value="'+city.nome+'">'+city.nome+'</option>');
        
                });
        
            },'json');

        }

    });


}


if(  $('#main-content #page-search').length > 0  ){

    $('.btn-delete').on('click',function(){

        $('#modal-delete').modal('show');

        let dataURL = $(this).attr('data-url');

        $('#yesGo').attr('data-url',dataURL);

    });

    $('#yesGo').on('click',function(){

        let dataURL = $(this).attr('data-url');

        if( dataURL !== null && dataURL !== undefined ){

            window.location = dataURL;

        }

    });

}